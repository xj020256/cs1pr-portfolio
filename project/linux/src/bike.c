#include "common.h"

static void tick(void);
static void touch(Entity *other);

void initBike()
{
    Entity *e;

	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;
    e->health = 1;
	e->hidden = 1;

	e->texture = loadTexture("gfx/bike.png");
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	e->flags = EF_WEIGHTLESS;
	e->tick = tick;
	e->touch = touch;


    e->dx = BIKE_SPEED;
    e->dy = 0;

    return;
}

static void tick(void)
{
    unsigned int current_time; 
	static unsigned int last_time = 0;

	current_time = SDL_GetTicks() / 1000;

	if( self->hidden  && (current_time > last_time + 5) )
	{
		self->hidden = 0;
		last_time = current_time;
        self->x = 0;
        self->y = player->y;
	}
	else if( current_time > last_time + 15 )
	{
		self->hidden = 1;
		last_time = current_time;
	}
    else
    {
        self->x += BIKE_SPEED;
    }
	
	self->value += 0.1;

	
}

static void touch(Entity *other)
{
    if (!self->hidden && other == player)
	{
		self->hidden = 1;

		stage.totalScore -= 5;
	}

}