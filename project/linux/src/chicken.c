#include "common.h"

static void tick(void);
static void touch(Entity *other);

void initChicken()
{
    Entity *e;

	e = malloc(sizeof(Entity));
	memset(e, 0, sizeof(Entity));
	stage.entityTail->next = e;
	stage.entityTail = e;
	e->health = 1;
	e->hidden = 1;

	e->texture = loadTexture("gfx/chicken.png");
	SDL_QueryTexture(e->texture, NULL, NULL, &e->w, &e->h);
	e->flags = EF_WEIGHTLESS;
	e->tick = tick;
	e->touch = touch;

    return;
}

static void tick(void)
{
	unsigned int current_time; 
	static unsigned int last_time = 0;

	current_time = SDL_GetTicks() / 1000;

	if( self->hidden  && (current_time > last_time + 5) )
	{
		self->hidden = 0;
		last_time = current_time;
		int temp = (rand() % 10) - 5;
		if ( temp > 0 )
		{
			self->x = player->x + 50 + ((rand() % 50) - 25);
			self->y = player->y + 50 + ((rand() % 50) - 25);
		}
		else
		{
			self->x = player->x - 50 + ((rand() % 50) - 25);
			self->y = player->y - 50 + ((rand() % 50) - 25);
		}
	}
	else if( current_time > last_time + 15 )
	{
		self->hidden = 1;
		last_time = current_time;
	}
	else
    {
        self->x += (rand() % CHICKEN_SPEED) - CHICKEN_SPEED / 2;
		self->y += (rand() % CHICKEN_SPEED) - CHICKEN_SPEED / 2;
    }

	self->value += 0.1;

	self->y += sin(self->value);
}

static void touch(Entity *other)
{
	if (!self->hidden && other == player)
	{
		self->hidden = 1;

		stage.totalScore += 100;
	}
    
}